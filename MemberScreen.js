import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  RefreshControl,
  DeviceEventEmitter,
  TouchableHighlight,
} from 'react-native';

import Container from './Components/Container';
import History from './Components/History';
import InformationPanel from './Components/InformationPanel';
import ReloadPuller from './Components/ReloadPuller';
import TreasuryModel from './Model/TreasuryModel';
import { MemberWallet } from './NativeModules';
import { TESTNET, LOCAL } from './Constants';

import { BallIndicator } from 'react-native-indicators';

import Modal from "react-native-modal";

export default class MemberScreen extends Component<{}> {
  static navigationOptions = {
    header: null,
  }

  state = {
    history: [],
    creator: null,
    treasurer: null,
    limit: 0,
    balance: 0,
    address: '--',
    ready: false,
    reloading: false,
    keyDataAvailable: false,
    firstTimeRun: false,
    downloading: false,
    syncing: false,
    downloadPercent: 0,
  }

  constructor(props) {
    super(props);
    this.model = new TreasuryModel();
    MemberWallet.useTestNet(TESTNET);
    MemberWallet.useLocal(LOCAL);
  }

  componentWillMount() {
    const { params } = this.props.navigation.state;
    if (params) {
      this.treasuryId = params.treasury._id;
      let creatorId = params.treasury.created_by;
      let treasurerId = params.treasury.treasurer;
      let addresses = params.treasury.addresses;
      let lastAddress = "--";
      let keyData = params.treasury.key_data;
      if (keyData) {
        this.setState({
          keyDataAvailable: true,
        });
      }
      if (addresses.length > 0) {
        lastAddress = addresses[addresses.length - 1];
      }
      this.model.getUserDetails(creatorId).then((creator) => {
        this.setState({
          creator: creator.name,
        });
      }, (e) => {
        console.log(e);
      });
      this.model.getUserDetails(treasurerId).then((treasurer) => {
        this.setState({
          treasurer: treasurer.name,
        });
      }, (e) => {
        console.log(e);
      });
      this.setState({
        history: params.treasury.history,
        limit: params.treasury.spending_limit,
        balance: params.treasury.balance,
        address: lastAddress,
      });
    }
  }

  componentDidMount() {
    MemberWallet.firstTimeRun().then((result) => {
      let firstTimeRun = result.firstTimeRun;
      this.setState({
        firstTimeRun: firstTimeRun,
      });
    }, (e) => {
      console.log(e);
    });
    MemberWallet.initiateSync(this.treasuryId).then(() => {
      this.setState({
        syncing: false,
        ready: true,
        firstTimeRun: false,
      });
    }, (e) => {
      console.log(e);
    });
    DeviceEventEmitter.addListener('chainStart', (e) => {
      this.setState({
        downloading: true,
      });
    });
    DeviceEventEmitter.addListener('chainCompleted', (e) => {
      this.setState({
        downloading: false,
        syncing: true,
      });
    });
    DeviceEventEmitter.addListener('chainPercent', (e) => {
      let percent = e.percent;
      this.setState({
        downloadPercent: percent.toFixed(2),
      });
    });
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeAllListeners();
  }

  _mBTC(satoshis) {
    if (satoshis > 0) {
      return (Math.abs(satoshis)/100000).toFixed(1);
    } else {
      return '--';
    }
  }

  _getLastAddress = (addresses) => {
    let lastAddress = "--";
    if (addresses.length > 0) {
      lastAddress = addresses[addresses.length - 1];
    }
    return lastAddress;
  }

  _reloadHandler = () => {
    return new Promise((resolve, reject) => {
      this.setState({
        reloading: true,
        ready: false,
      });
      MemberWallet.useTestNet(TESTNET).then(() => {
        MemberWallet.initiateSync(this.treasuryId).then(() => {
          this.model.getTreasuryDetails(this.treasuryId).then((treasury) => {
            let history = treasury.history;
            let balance = treasury.balance;
            let lastAddress = this._getLastAddress(treasury.addresses);
            let keyDataAvailable = false;
            if (treasury.key_data) {
              keyDataAvailable = true;
            }
            this.setState({
              history: history,
              address: lastAddress,
              balance: balance,
              keyDataAvailable: keyDataAvailable,
              reloading: false,
              syncing: false,
              ready: true,
            });
            resolve();
          }, (e) => {
            reject(e);
          });
        }, (e) => {
          reject(e);
        });
      }, (e) => {
        reject(e);
      });
    });
  }

  _onRefresh = () => {
    if (this.state.ready) {
      this._reloadHandler().then(() => {}, (e) => {
        console.log(e)
      });
    }
  }

  _refreshTreasury = () => {
    _reloadHandler().then(() => {}, (e) => {
      console.log(e);
    });
  }

  render() {
    let history = this.state.history;
    let creator = this.state.creator;
    let treasurer = this.state.treasurer;
    let balance = this._mBTC(this.state.balance);
    let limit = this._mBTC(this.state.limit);
    let address = this.state.address;

    let historyView;
    if (history.length > 0) {
      historyView = (
        <History list={history} reloadHandler={this._reloadHandler} />
      );
    } else {
      historyView = (
        <ScrollView contentContainerStyle={styles.startView}
          refreshControl={
            <RefreshControl
            refreshing={this.state.reloading}
            onRefresh={this._onRefresh}
            />
          }
        >
          <Text style={styles.startText}>
            Send money to the above address to get started
          </Text>
        </ScrollView>
      );
    }

    let ready = this.state.ready;
    let notReadySetupText = "Treasurer has not yet created a wallet to deposit funds";

    let keyDataAvailable = this.state.keyDataAvailable;
    let notReadyView = (
      <Modal isVisible={!keyDataAvailable}
        onBackdropPress={() => {this.setState({ready: true})}}> 
        <View style={styles.notReadySetup}>
          <View style={styles.setupInformation}>
            <Text style={styles.notReadySetupText}>
              {notReadySetupText}
            </Text>
          </View>
          <TouchableHighlight style={styles.button} onPress={this._refreshTreasury}>
            <View style={{transform: [{scaleX: 1/2}]}}>
              <Text style={styles.buttonText}>Check again</Text>
            </View>
          </TouchableHighlight>
        </View>
      </Modal>
    );

    let reloading = this.state.reloading;
    let reloader;
    if (!reloading) {
      reloader = (
        <ReloadPuller />
      );
    }

    let firstTimeRun = this.state.firstTimeRun;
    let setupText = null;
    let downloading = this.state.downloading;
    let syncing = this.state.syncing;
    if (ready) {
      setupText = "Ready";
    }
    else {
      if (downloading) {
        let downloadPercent = parseInt(this.state.downloadPercent, 10);
        setupText = downloadPercent + "% downloaded";
      } else if (syncing) {
        setupText = "Syncing with treasury";
      } else if (firstTimeRun) {
        setupText = "Setting things up for the first time";
      } else {
        setupText = "Connecting with peers...";
      }
    }

    let firstTimeView = (
      <Modal isVisible={!ready && firstTimeRun && keyDataAvailable}
        backdropOpacity={0.8}
        onBackdropPress={() => {this.setState({ready: true})}}> 
        <View style={styles.setup}>
          <BallIndicator size={40} color='white' />
          <View style={styles.setupInformation}>
            <Text style={styles.firstTimeSetupText}>
              {setupText}
            </Text>
          </View>
        </View>
      </Modal>
    );

    return (
      <Container>
        <InformationPanel
          creator={creator}
          treasurer={treasurer}
          balance={balance}
          limit={limit}
          address={address}
        />
        {reloader}
        <View style={{flex: 1}}>
          {historyView}
        </View>
        <View style={styles.statusBar}>
          <Text style={styles.setupText}>
            {setupText}
          </Text>
        </View>
        {notReadyView}
        {firstTimeView}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  informationPanel: {
    backgroundColor: 'black',
    height: 100,
    padding: 10,
    borderBottomWidth: 1,
    borderColor: 'white',
    flexDirection: 'row',
  },
  label: {
    marginRight: 3,
    color: 'white',
    fontSize: 18,
    fontFamily: 'PoiretOne-Regular',
  },
  text: {
    fontSize: 22,
    color: 'white',
  },
  treasuryInformation: {
    flex: 1,
  },
  accountInformation: {
    flex: 1,
  },
  labelTextPair: {
    flexDirection: 'row',
  },
  startView: {
    backgroundColor: 'black',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20,
    height: 80,
  },
  startText: {
    color: 'white',
  },
  setupInformation: {
    flex: 1,
    alignItems: 'center',
  },
  notReadySetup: {
    height: 100,
    padding: 10,
    borderRadius: 10,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  notReadySetupText: {
    textAlign: 'center',
  },
  button: {
    justifyContent: 'center',
    backgroundColor: '#0EBFE9',
    width: 80,
    height: 25,
    borderRadius: 50,
    margin: 10,
    transform: [
      {scaleX: 2}
    ]
  },
  buttonText: {
    color: 'white',
  },
  setup: {
    height: 400,
  },
  firstTimeSetupText: {
    fontSize: 18,
    textAlign: 'center',
    color: 'white',
  },
  statusBar: {
    height: 20,
    paddingLeft: 10,
    paddingTop: 2,
    paddingBottom: 2,
    backgroundColor: '#222',
  },
  setupText: {
    color: '#DDD',
    fontSize: 10,
  },

});
