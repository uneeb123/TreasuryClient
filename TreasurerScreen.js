import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  RefreshControl,
  DeviceEventEmitter,
} from 'react-native';

import Container from './Components/Container';
import GoldenButton from './Components/GoldenButton';
import History from './Components/History';
import InformationPanel from './Components/InformationPanel';
import ReloadPuller from './Components/ReloadPuller';
import TreasuryModel from './Model/TreasuryModel';
import { TreasuryCore } from './NativeModules';
import { TESTNET, LOCAL } from './Constants';

import { BallIndicator } from 'react-native-indicators';
import Modal from "react-native-modal";

export default class TreasurerScreen extends Component<{}> {
  static navigationOptions = {
    header: null,
  }

  state = {
    history: [],
    creator: null,
    treasurer: null,
    limit: 0,
    balance: 0,
    address: '--',
    ready: false,
    firstTimeRun: false,
    downloading: false,
    syncing: false,
    downloadPercent: 0,
    reloading: false,
  }

  constructor(props) {
    super(props);
    this.model = new TreasuryModel();
    TreasuryCore.useTestNet(TESTNET);
    TreasuryCore.useLocal(LOCAL);
  }

  componentWillMount() {
    const { params } = this.props.navigation.state;
    if (params) {
      this.treasuryId = params.treasury._id;
      let creatorId = params.treasury.created_by;
      let treasurerId = params.treasury.treasurer;
      let addresses = params.treasury.addresses;
      let lastAddress = this._getLastAddress(addresses);
      this.model.getUserDetails(creatorId).then((creator) => {
        this.setState({
          creator: creator.name,
        });
      }, (e) => {
        console.log(e);
      });
      this.model.getUserDetails(treasurerId).then((treasurer) => {
        this.setState({
          treasurer: treasurer.name,
        });
      }, (e) => {
        console.log(e);
      });
      this.setState({
        history: params.treasury.history,
        limit: params.treasury.spending_limit,
        balance: params.treasury.balance,
        address: lastAddress,
      });
    }
  }

  componentDidMount() {
    TreasuryCore.firstTimeRun().then((result) => {
      let firstTimeRun = result.firstTimeRun;
      this.setState({
        firstTimeRun: firstTimeRun,
      });
    }, (e) => {
      console.log(e);
    });
    TreasuryCore.initiateSync(this.treasuryId).then(() => {
      this.setState({
        syncing: false,
        ready: true,
      });
      let firstTimeRun = this.state.firstTimeRun;
      if (firstTimeRun) {
        TreasuryCore.freshAddress().then((result) => {
          let newAddress = result.address;
          this.setState({
            address: newAddress,
          });
        }, (e) => {
          console.log(e);
        });
      }
    }, (e) => {
      console.log(e);
    });
    DeviceEventEmitter.addListener('chainStart', (e) => {
      this.setState({
        downloading: true,
      });
    });
    DeviceEventEmitter.addListener('chainCompleted', (e) => {
      this.setState({
        downloading: false,
        syncing: true,
      });
    });
    DeviceEventEmitter.addListener('chainPercent', (e) => {
      let percent = e.percent;
      this.setState({
        downloadPercent: percent.toFixed(2),
      });
    });
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeAllListeners();
  }

  _handleSpend = () => {
    const { navigate } = this.props.navigation;
    navigate('Spend');
  }

  _mBTC(satoshis) {
    if (satoshis > 0) {
      return (Math.abs(satoshis)/100000).toFixed(1);
    } else {
      return '--';
    }
  }

  _getLastAddress = (addresses) => {
    let lastAddress = "--";
    if (addresses.length > 0) {
      lastAddress = addresses[addresses.length - 1];
    }
    return lastAddress;
  }

  _reloadHandler = () => {
    return new Promise((resolve, reject) => {
      this.setState({
        reloading: true,
      });
      this.model.getTreasuryDetails(this.treasuryId).then((treasury) => {
        let history = treasury.history;
        let balance = treasury.balance;
        let lastAddress = this._getLastAddress(treasury.addresses);
        this.setState({
          history: history,
          balance: balance,
          address: lastAddress,
          reloading: false,
        });
        resolve();
      }, (e) => {
        reject(e);
      });
    });
  }

  _onRefresh = () => {
    if (this.state.ready) {
      this.setState({
        ready: false,
        reloading: true,
      });
      TreasuryCore.initiateSync(this.treasuryId).then(() => {
        this.setState({
          syncing: false,
          ready: true,
        });
        this._reloadHandler().then(() => {}, (e) => {
          console.log(e)
        });
      }, (e) => {
        console.log(e);
      });
    }
  }

  render() {
    let ready = this.state.ready;
    let history = this.state.history;
    let creator = this.state.creator;
    let treasurer = this.state.treasurer;
    let balance = this._mBTC(this.state.balance);
    let limit = this._mBTC(this.state.limit);
    let address = this.state.address;

    let historyView;
    if (history.length > 0) {
      historyView = (
        <History list={history} reloadHandler={this._reloadHandler}/>
      );
    } else {
      historyView = (
        <ScrollView contentContainerStyle={styles.startView}
          refreshControl={
            <RefreshControl
            refreshing={this.state.reloading}
            onRefresh={this._onRefresh}
            />
          }
        >
          <Text style={styles.startText}>
            Send money to the above address to get started
          </Text>
        </ScrollView>
      );
    }
    
    let firstTimeRun = this.state.firstTimeRun;
    let setupText = null;
    let downloading = this.state.downloading;
    let syncing = this.state.syncing;
    if (ready) {
      setupText = "Ready";
    }
    else {
      if (downloading) {
        let downloadPercent = parseInt(this.state.downloadPercent, 10);
        setupText = downloadPercent + "% downloaded";
      } else if (syncing) {
        setupText = "Syncing with treasury";
      } else if (firstTimeRun) {
        setupText = "Setting things up for the first time";
      } else {
        setupText = "Connecting with peers...";
      }
    }

    let firstTimeView = (
      <Modal isVisible={!ready && firstTimeRun}
        backdropOpacity={0.8}>
        <View style={styles.setup}>
          <BallIndicator style={styles.setupIndicator} size={40} color='white' />
          <View style={styles.setupInformation}>
            <Text style={styles.firstTimeSetupText}>
              {setupText}
            </Text>
          </View>
        </View>
      </Modal>
    );

    let spendText;
    if (ready) {
      spendText = "Spend";
    } else {
      spendText = "Please wait";
    }

    let reloading = this.state.reloading;
    let reloader;
    if (!reloading) {
      reloader = (<ReloadPuller />);
    }

    return (
      <Container>
        <InformationPanel 
          creator={creator}
          treasurer={treasurer}
          balance={balance}
          limit={limit}
          address={address} />
        {reloader}
        <View style={{flex: 1}}>
          {historyView}
        </View>
        <View style={styles.buttonContainer}>
          <GoldenButton onPress={this._handleSpend} disabled={!ready}>
            <Text style={styles.buttonText}>{spendText}</Text>
          </GoldenButton>
        </View>
        <View style={styles.statusBar}>
          <Text style={styles.setupText}>
            {setupText}
          </Text>
        </View>
        {firstTimeView}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  buttonContainer: {
    alignItems: 'center',
  },
  buttonText: {
    textAlign: 'center',
    color: 'white',
  },
  setupInformation: {
    flex: 1,
    alignItems: 'center',
  },
  setupIndicator: {
//    justifyContent: 'flex-end',
  },
  setup: {
    height: 400,
  },
  firstTimeSetupText: {
    fontSize: 18,
    textAlign: 'center',
    color: 'white',
  },
  startView: {
    backgroundColor: 'black',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20,
    height: 80,
  },
  startText: {
    color: 'white',
  },
  statusBar: {
    height: 20,
    paddingLeft: 10,
    paddingTop: 2,
    paddingBottom: 2,
    backgroundColor: '#222',
  },
  setupText: {
    color: '#DDD',
    fontSize: 10,
  },
});
