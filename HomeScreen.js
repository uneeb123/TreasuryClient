import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  RefreshControl,
  Button
} from 'react-native';

import Treasuries from './Components/Treasuries';
import Container from './Components/Container';
import GoldenButton from './Components/GoldenButton';
import NewTreasury from './Components/NewTreasury';
import TreasuryModel from './Model/TreasuryModel';
import ReloadPuller from './Components/ReloadPuller';
import FirstTime from './Components/FirstTime';

export default class HomeScreen extends Component<{}> {
  static navigationOptions = {
    header: null,
  }

  constructor(props) {
    super(props);
    this.model = new TreasuryModel();
    const { params } = this.props.navigation.state;
    let empty = this._emptyScreen(params.treasuries);
    this.state = {
      user: params.user,
      treasuries: params.treasuries,
      newDialog: false,
      empty: empty,
      reloading: false,
      firstTimeUser: params.firstTimeUser,
    };
  }

  _reload = () => {
    return new Promise((resolve, reject) => {
      this.setState({
        reloading: true,
      });

      this.model.getDetailedInformation().then((details) => {
        let empty = this._emptyScreen(details.treasuries);
        this.setState({
          user: details.user,
          treasuries: details.treasuries,
          reloading: false,
          empty: empty,
        });
        resolve();
      }, (e) => {
        reject(e);
      });
    });
  }

  _showCreate = () => {
    this.setState({
      newDialog: true,
    });
  }

  _cancelCreate = () => {
    this.setState({
      newDialog: false,
    });
  }

  _submitCreate = (treasurer, members, amount, cadence) => {
    members.push(treasurer);
    this.setState({
      newDialog: false,
    });
    this.model.createTreasury(members, treasurer, amount).then((treasuryId) => {
      this._reload();
    }, (e) => {
      console.log(e);
    });
  }

  _emptyScreen = (treasuries) => {
    let empty = false;
    let totalReadyTreasuries = treasuries.ready_treasuries.length;
    let totalPendingTreasuries = treasuries.pending_treasuries.length;
    let totalInvitedTreasuries = treasuries.invited_treasuries.length;
    if (totalReadyTreasuries == 0
      && totalPendingTreasuries == 0
      && totalInvitedTreasuries == 0) {
      empty = true;
    }
    return empty;
  }
  
  render() {
    let name = this.state.user.name;
    let ready_treasuries = [];
    let invited_treasuries = [];
    let pending_treasuries = [];
    if (this.state.treasuries) {
      ready_treasuries = this.state.treasuries.ready_treasuries;
      invited_treasuries = this.state.treasuries.invited_treasuries;
      pending_treasuries = this.state.treasuries.pending_treasuries;
    }
    let showNewDialog = this.state.newDialog;
    let empty = this.state.empty;
    let firstTimeUser = this.state.firstTimeUser;

    let treasuriesView = (
      <Treasuries
        reloadHandler={this._reload}
        ready={ready_treasuries}
        invited={invited_treasuries}
        pending={pending_treasuries} />
    );
    if (empty) {
      treasuriesView = (
        <ScrollView contentContainerStyle={styles.startView}
          refreshControl={
            <RefreshControl
            refreshing={this.state.reloading}
            onRefresh={this._reload}
            />
          }
        >
          <Text style={styles.startText}>
            Create a new Treasury to get started
          </Text>
        </ScrollView>
      );
    }

    let reloading = this.state.reloading;
    let reloader;
    if (!reloading) {
      reloader = (<ReloadPuller />);
    }

    return (
      <Container>
        <View style={styles.welcome}>
          <View style={styles.welcomeContainer}>
            <Text style={styles.welcomeText}>Welcome,</Text>
          </View>
          <View style={styles.nameContainer}>
            <Text style={styles.nameText}>{name}</Text>
          </View>
        </View>
        {reloader}
        {treasuriesView}
        <View style={{alignItems: 'center'}}>
          <GoldenButton onPress={this._showCreate}>
            <Text style={styles.buttonText}>New</Text>
          </GoldenButton>
        </View>
        <FirstTime
          isVisible={firstTimeUser}
          handleAccept={() => {this.setState({firstTimeUser: false})}}/>
        <NewTreasury
          isVisible={showNewDialog}
          onBackdropPress={this._cancelCreate}
          handleCancel={this._cancelCreate}
          handleSubmit={this._submitCreate}
          avoidKeyboard={true} />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  buttonText: {
    textAlign: 'center',
    color: 'white',
  },
  welcome: {
    backgroundColor: 'black',
    paddingLeft: 10,
    paddingTop: 10,
    paddingBottom: 20,
    flexDirection: 'row',
    alignItems: 'flex-end',
    borderBottomWidth: 1,
    borderColor: 'white',
  },
  welcomeText: {
    color: 'white',
    fontSize: 30,
    fontFamily: 'PoiretOne-Regular',
  },
  nameText: {
    color: 'white',
    fontFamily: 'PoiretOne-Regular',
    fontSize: 20,
  },
  welcomeContainer: {
  },
  nameContainer: {
    marginLeft: 10,
    marginBottom: 1,
  },
  startView: {
    backgroundColor: 'black',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20,
    height: 80,
  },
  startText: {
    color: 'white',
  },
});
