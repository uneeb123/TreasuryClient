import {NativeModules} from 'react-native';

module.exports = {
  PhoneVerification: NativeModules.PhoneVerification,
  TreasuryCore: NativeModules.TreasuryCore,
  MemberWallet: NativeModules.MemberWallet
}
