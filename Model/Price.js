export default class Price {
  constructor() {
    this.baseUrl = "https://blockchain.info";
  }

  getBitcoinPrice() {
    var dayPriceUrl = this.baseUrl + "/q/24hrprice";
    return new Promise((resolve, reject) => {
      fetch(dayPriceUrl).then((response) => {
        response.json().then((json) => {
          resolve(json);
        }, (e) => {
          reject(e);
        });
      }, (e) => {
        reject(e);
      });
    });
  }

  satoshiToUSD(satoshis) {
    return new Promise((resolve, reject) => {
      this.getBitcoinPrice().then((price) => {
        let btc = satoshis/100000000;
        let usd = (price*btc).toFixed(2);
        resolve(usd);
      }, (e) => {
        reject(e);
      });
    });
  }
}
