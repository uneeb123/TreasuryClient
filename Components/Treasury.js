import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Alert,
} from 'react-native';
import { withNavigation } from 'react-navigation';

import TreasuryModel from '../Model/TreasuryModel';
import Price from '../Model/Price';

import Icon from 'react-native-vector-icons/FontAwesome';
import * as Animatable from 'react-native-animatable';

class Treasury extends Component<{}> {
  state = {
    balanceUSD: '--',
    limitUSD: '--',
  };

  constructor(props) {
    super(props);
    this.invited = this.props.invited || false;
    this.pending = this.props.pending || false;
    this.ready = this.props.ready || false;
    this.model = new TreasuryModel();
  }

  componentWillMount() {
    this.model.getUser().then((me) => {
      if (me._id == this.props.treasury.treasurer) {
        this.setState({
          treasurer: true,
        });
      }
    }, (e) => {
      console.log(e);
    });
    let balance = this.props.treasury.balance;
    let limit = this.props.treasury.spending_limit;
    this._updateUSDConversion(balance, limit);
  }

  componentWillReceiveProps(nextProps) {
    let balance = nextProps.treasury.balance;
    let limit = nextProps.treasury.spending_limit;
    this._updateUSDConversion(balance, limit);
  }

  _updateUSDConversion = (balance, limit) => {
    var price = new Price();
    if (balance > 0) {
      price.satoshiToUSD(balance).then((usd) => {
        this.setState({
          balanceUSD: usd,
        });
      }, (e) => {
        console.log(e);
      });
    } else if (balance == 0) {
      this.setState({
        balanceUSD: '--',
      });
    }
    price.satoshiToUSD(limit).then((usd) => {
      this.setState({
        limitUSD: usd,
      });
    }, (e) => {
      console.log(e);
    });
  }

  _acceptInvite = () => {
    let treasuryId = this.props.treasury._id;
    this.model.getUserId().then((id) => {
      let memberId = id;
      this.model.acceptInvite(memberId, treasuryId).then((ready) => {
        this.props.reloadHandler();
      }, (e) => {
        console.log(e);
      });
    }, (e) => {
      console.log(e);
    });
  }

  _handleEvent = () => {
    if (this.invited) {
      let limit = this.props.treasury.spending_limit;
      Alert.alert(
        'Accept invite?',
        "The spending limit is set to " + this._mBTC(limit) + " mBTC per day",
        [
          {text: 'Not now', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'Yes', onPress: this._acceptInvite},
        ],
        { cancelable: false }
      );
    } else if (this.pending) {
      Alert.alert(
        'Treasury',
        'Not all invited members have accepted invite',
        [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        { cancelable: false }
      );
    } else if (this.ready) {
      const { navigate } = this.props.navigation;
      if (this.state.treasurer) {
        navigate('Treasurer', {
          treasury: this.props.treasury
        });
      } else {
        navigate('Member', {
          treasury: this.props.treasury
        });
      }
    } else {
      Alert.alert("You clicked something wrong");
    }
  }

  _light() {
    if (this.invited) {
      return (
        <Animatable.View animation="flash" iterationCount="infinite" easing="linear" iterationDelay={0} style={styles.statusLight}>
          <Icon name="circle" size={10} color='red'/>
        </Animatable.View>
      );
    } else if (this.pending) {
      return (<Icon name="circle" size={10} color='red' style={styles.statusLight} />);
    } else if (this.ready) {
      return (<Icon name="circle" size={10} color='green' style={styles.statusLight} />);
    } else {
      return null;
    }
  }

  _members() {
    let memberLength = this.props.treasury.members.length;
    let members = [];
    members.push(<Icon key="0" name="child" size={20}/>);
    for (var i = 0; i < memberLength; i++) {
      var key = (i+1).toString();
      members.push(<Icon key={key} name="child" size={20}/>);
    }
    return members;
  }

  _BTC(satoshis) {
    if (satoshis > 0) {
      return (Math.abs(satoshis)/100000000).toFixed(4);
    } else {
      return '--';
    }
  }

  _mBTC(satoshis) {
    if (satoshis > 0) {
      return (Math.abs(satoshis)/100000).toFixed(4);
    } else {
      return '--';
    }
  }

  render() {
    let balance = this.props.treasury.balance;
    let balanceUSD = this.state.balanceUSD;
    let limitUSD = this.state.limitUSD;
    let limit = this.props.treasury.spending_limit;
    let memberLength = this.props.treasury.members.length;

    return (
      <TouchableOpacity onPress={this._handleEvent} style={styles.treasuryContainer}>
        {this._light()}
        <View style={styles.treasuryInformation}>
          <View style={styles.treasuryInformationTopPanel}>
          <View style={styles.accountInformation}>
            <View style={styles.btc}>
              <Icon name="btc" color='white' size={20}/>
              <Text style={styles.btcValue}>{this._BTC(balance)}</Text>
            </View>
            <View style={styles.usd}>
              <Icon name="dollar" color='white' size={20}/>
              <Text style={styles.usdValue}>{balanceUSD}</Text>
            </View>
          </View>
          <View style={styles.userInformation}>
            {this._members()}
          </View>
          </View>
          <Text style={styles.limitText}>Spending limit is set to {this._mBTC(limit)} mBTC ({limitUSD} USD) per day.</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default withNavigation(Treasury);

const styles = StyleSheet.create({
  treasuryContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    padding: 10,
    backgroundColor: 'rgba(255,215,0,0.8)',
    borderRadius: 10,
  },
  statusLight: {
    flex: 1,
  },
  treasuryInformation: {
    flex: 9,
  },
  treasuryInformationTopPanel: {
    flexDirection: 'row',
  },
  limitText: {
    color: '#FFF',
    fontSize: 12,
  },
  id: {
    fontSize: 10,
    color: '#777',
  },
  userInformation: {
    flex: 1,
    flexDirection: 'row',
  },
  accountInformation: {
    flex: 1,
  },
  btc: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  usd: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  btcValue: {
    marginLeft: 5,
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
  },
  usdValue: {
    marginTop: 3,
    marginLeft: 5,
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
  }
});
