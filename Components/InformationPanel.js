import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  TouchableOpacity,
  Clipboard,
  ToastAndroid,
  StyleSheet,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

export default class InformationPanel extends Component<{}> {
  _copyAddress = () => {
    Clipboard.setString(this.props.address);
    ToastAndroid.show('Address copied!', ToastAndroid.SHORT);
  }

  render() {
    let creator = this.props.creator;
    let treasurer = this.props.treasurer;
    let balance = this.props.balance;
    let limit = this.props.limit;
    let address = this.props.address;

    return (
      <View style={styles.informationPanel}>
        <View style={styles.treasuryAccount}>
          <View style={styles.treasuryInformation}>
            <View style={styles.labelTextPair}>
              <Text style={styles.label}>creator</Text>
              <Text style={styles.text}>{creator}</Text>
            </View>
            <View style={styles.labelTextPair}>
              <Text style={styles.label}>treasurer</Text>
              <Text style={styles.text}>{treasurer}</Text>
            </View>
          </View>
          <View style={styles.accountInformation}>
            <View style={styles.labelTextPair}>
              <Text style={styles.label}>balance</Text>
              <Text style={styles.text}>{balance} mBTC</Text>
            </View>
            <View style={styles.labelTextPair}>
              <Text style={styles.label}>limit</Text>
              <Text style={styles.text}>{limit} mBTC</Text>
            </View>
          </View>
          {this.props.children}
        </View>
        <View style={styles.addressInformation}>
          <Text style={styles.label}>address</Text>
          <TextInput
            editable={false}
            selectionColor={'#AAA'}
            underlineColorAndroid={'transparent'}
            style={styles.addressText}
            value={address}
          />
          <TouchableOpacity style={styles.copyButton} onPress={this._copyAddress}>
            <Icon name="copy" size={20} color='black' />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  informationPanel: {
    backgroundColor: 'black',
    height: 120,
    padding: 10,
    borderBottomWidth: 1,
    borderColor: 'white',
  },
  treasuryAccount: {
    flexDirection: 'row',
    marginBottom: 5,
  },
  label: {
    marginRight: 3,
    color: 'white',
    fontSize: 18,
    fontFamily: 'PoiretOne-Regular',
  },
  text: {
    fontSize: 16,
    color: 'white',
  },
  treasuryInformation: {
    flex: 3,
  },
  accountInformation: {
    flex: 3,
  },
  labelTextPair: {
    flexDirection: 'row',
  },
  addressInformation: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  addressText: {
    flex: 1,
    backgroundColor: 'white',
    height: 35,
    fontFamily: 'monospace',
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: 'white',
    color: '#777',
    borderRadius: 5,
  },
  copyButton: {
    paddingTop: 7,
    paddingBottom: 7,
    paddingLeft: 3,
    paddingRight: 3,
    borderRadius: 3,
    backgroundColor: '#EEE',
    marginRight: 2,
    marginLeft: 2,
  }
});
