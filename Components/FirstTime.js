import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  StyleSheet
} from 'react-native';

import Modal from "react-native-modal";

const header = "Treasury v0.1 (Pre-alpha)";

const agreement = "Welcome to Treasury! Treasury is a Bitcoin wallet that is designed to " +  
"give users complete transparency and autonomy over the transferred funds. " + 
"Treasury is an account that is jointly owned by multiple parties. Each treasury " +
"has a designated treasurer who has the privilege of spending funds. Members of the " +
"account has visibility into all the activities.\n\nTo get started, create a new " +
"treasury by inviting members using their phone numbers and assign one member as a treasurer. " +
"You also have to specify the amount that a treasurer is allowed to spend per day.\n\n" +
"Keep in mind that this is an early release and there might be potential issues. Use with " +
"caution!";

export default class FirstTime extends Component<{}> {
  render() {
    return (
      <Modal {...this.props}>
        <View style={styles.modalContainer}>
          <View style={styles.contentWrap}>
            <Text style={styles.header}>{header}</Text>
            <Text style={styles.agreement}>{agreement}</Text>
          </View>
          <TouchableHighlight style={styles.button} onPress={this.props.handleAccept}>
            <View style={{transform: [{scaleX: 1/2}]}}>
              <Text style={styles.buttonText}>{'Accept and continue'}</Text>
            </View>
          </TouchableHighlight>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 10,
  },
  contentWrap: {
    margin: 10,
  },
  button: {
    justifyContent: 'center',
    backgroundColor: '#0EBFE9',
    width: 150,
    height: 30,
    borderRadius: 50,
    margin: 20,
    transform: [
      {scaleX: 2}
    ]
  },
  buttonText: {
    color: 'white',
  },
  header: {
    fontWeight: 'bold',
    fontSize: 18,
    marginBottom: 20,
  },
  agreement: {
    fontSize: 12,
  }
});
