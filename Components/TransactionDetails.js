import React, { Component } from 'react';
import {
  View,
  Text,
  Button,
  Linking,
  TouchableHighlight,
  StyleSheet
} from 'react-native';

import Modal from "react-native-modal";
import Icon from 'react-native-vector-icons/FontAwesome';

import Price from '../Model/Price';
import { TESTNET, LOCAL } from '../Constants';

export default class TransactionDetails extends Component<{}> {
  state = {
    amountUSD: '--',
    feeUSD: '--',
  }

  componentWillReceiveProps(props) {
    if (props.transaction) {
      let amount = props.transaction.amount;
      let fee = props.transaction.fee;
      this._getUSD(amount, fee);
    }
  }

  _goToURL = () => {
    let transactionUrl;
    if (TESTNET) {
      transactionUrl = "https://testnet.blockchain.info/tx/";
    } else {
      transactionUrl = "https://blockchain.info/tx/";
    }
    let url = transactionUrl + this.props.transaction.tx_id;
    Linking.openURL(url);
  }

  _mBTC(satoshis) {
    return (Math.abs(satoshis)/100000).toFixed(2);
  }

  _getUSD(amount, fee) {
    let price = new Price();
    price.satoshiToUSD(amount).then((usd) => {
      this.setState({
        amountUSD: usd,
      });
    }, (e) => {
      console.log(e);
    });
    price.satoshiToUSD(fee).then((usd) => {
      this.setState({
        feeUSD: usd,
      });
    }, (e) => {
      console.log(e);
    });
  }

  render() {
    let transaction = this.props.transaction;
    let amount;
    let amountUSD;
    let amountCurrentUSD;
    let address;
    let id;
    let date = new Date();
    let fee;
    let feeUSD;
    let incoming = false;
    if (transaction) {
      amount = this.props.transaction.amount;
      amountUSD = this.props.transaction.amount_usd;
      amountCurrentUSD = this.state.amountUSD;
      address = this.props.transaction.to_address;
      if (address == '0') {
        incoming = true;
      }
      let timestamp = this.props.transaction.timestamp;
      date = new Date(timestamp);
      fee = this.props.transaction.fee;
      feeUSD = this.state.feeUSD;
    }
    
    if (!incoming) {
      var addressView = (
        <View style={styles.addressContainer}>
          <Text style={styles.label}>{"Address: "}</Text>
          <Text style={styles.addressText}>{address}</Text>
        </View>
      );
    }
    
    let directionView;
    if (incoming) {
      directionView = (
        <View>
          <Text style={styles.direction}>Incoming transaction</Text>
        </View>
      );
    } else {
      directionView = (
        <View>
          <Text style={styles.direction}>Outgoing transaction</Text>
        </View>
      );
    }

    let displayAmount = (
      <Text>{this._mBTC(amount) + " mBTC"}</Text>
    );
    let displayAmountUSD = (
      <Text>{"$" + amountUSD + " ($" + amountCurrentUSD + " today)"}</Text>
    );
    let displayFee = (
      <Text>{this._mBTC(fee) + " mBTC ($" + feeUSD + " today)"}</Text>
    );

    return (
      <Modal {...this.props}>
        <View style={styles.modalContainer}>
          <View style={styles.informationContainer}>
            <Text style={styles.headerText}>Transaction details</Text>
            {directionView}
            <View style={styles.amountContainer}>
              <Text style={styles.label}>{"Amount: "}</Text>
              {displayAmount}
            </View>
            <View style={styles.amountUSDContainer}>
              <Text style={styles.label}>{"Amount (in USD): "}</Text>
              {displayAmountUSD}
            </View>
            {addressView}
            <View style={styles.dateContainer}>
              <Text style={styles.label}>{"Date: "}</Text>
              <Text style={styles.dateText}>{date.toString()}</Text>
            </View>
            <View style={styles.feeContainer}>
              <Text style={styles.label}>{"Fee: "}</Text>
              {displayFee}
            </View>
            <TouchableHighlight onPress={this._goToURL}>
              <View style={styles.detailsView}>
                <Text style={styles.detailsText}>more details</Text>
                <Icon name="external-link" size={10} />
              </View>
            </TouchableHighlight>
          </View>
          <TouchableHighlight style={styles.button} onPress={this.props.handleCancel}>
            <View style={{transform: [{scaleX: 1/2}]}}>
              <Text style={styles.buttonText}>Cancel</Text>
            </View>
          </TouchableHighlight>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    padding: 10,
    backgroundColor: 'white',
    borderRadius: 10,
    height: 300,
    alignItems: 'center',
  },
  headerText: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  amountContainer: {
    flexDirection: 'row',
  },
  amountUSDContainer: {
    flexDirection: 'row',
  },
  label: {
    fontWeight: 'bold',
  },
  amountText: {
  },
  addressContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  dateContainer: {
    flexDirection: 'row',
  },
  dateText: {
  },
  feeContainer: {
    flexDirection: 'row',
  },
  feeText: {
  },
  direction: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 10,
    marginBottom: 10,
  },
  addressText: {
    fontSize: 10,
    fontFamily: 'monospace'
  },
  detailsView: {
    marginTop: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  detailsText: {
    marginRight: 5,
    textDecorationLine: 'underline',
  },
  informationContainer: {
    flex: 1,
  },
  button: {
    justifyContent: 'center',
    backgroundColor: '#0EBFE9',
    width: 50,
    height: 25,
    borderRadius: 50,
    margin: 20,
    transform: [
      {scaleX: 2}
    ]
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly'
  },
  buttonText: {
    color: 'white',
  }
});
