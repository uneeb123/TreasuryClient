import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import * as Animatable from 'react-native-animatable';

export default class ReloadPuller extends Component<{}> {
  render() {
    return (
      <View style={styles.panel}>
        <Icon name="chevron-up" size={20} color='white' />
        <View style={styles.textContainer}>
          <Animatable.Text
            animation={bounce}
            iterationDelay={1000}
            easing="ease-out"
            iterationCount="infinite"
            style={styles.reloadText}>
            pull to refresh
          </Animatable.Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  reloadText: {
    color: 'white',
  },
  panel: {
    alignItems: 'center',
    marginTop: 5,
  },
  textContainer: {
    paddingTop: 3,
  }
});

const bounce = {
  0: {
    translateY: 0,
  },
  0.2: {
    translateY: 0,
  },
  0.4: {
    translateY: 0,
  },
  0.43: {
    translateY: 0,
  },
  0.53: {
    translateY: 0,
  },
  0.7: {
    translateY: -5,
  },
  0.8: {
    translateY: 0,
  },
  0.9: {
    translateY: -2,
  },
  1: {
    translateY: 0,
  },
};
