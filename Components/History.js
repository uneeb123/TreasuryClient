import React, { Component } from 'react';
import {
  View,
  FlatList,
  Text,
  StyleSheet,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';

import TransactionDetails from '../Components/TransactionDetails';

import Icon from 'react-native-vector-icons/FontAwesome';

class Panel extends Component<{}> {
  _mBTC (satoshis) {
    return (Math.abs(satoshis)/100000).toFixed(2);
  }

  _handleDetails = () => {
    this.props.detailsHandler(this.props.item);
  }

  render() {
    let positive = (<Icon name="chevron-up" size={30} color='green' />);
    let negative = (<Icon name="chevron-down" size={30} color='red' />);
    let direction = positive;
    if (this.props.item.amount < 0) {
      direction = negative;
    }

    let amount = this._mBTC(this.props.item.amount);
    let date = new Date(this.props.item.timestamp);

    return (
      <TouchableOpacity onPress={this._handleDetails} style={styles.panelContainer}>
        <View style={styles.directionContainer}>
          {direction}
        </View>
        <View style={styles.amountContainer}>
          <Text style={styles.amount}>{amount}</Text>
        </View>
        <View style={styles.labelContainer}>
          <Text style={styles.currencyLabel}>mBTC</Text>
        </View>
        <View style={styles.dateContainer}>
          <Text style={styles.date}>{date.toLocaleDateString('en-US')}</Text>
          <Text style={styles.date}>{date.toLocaleTimeString('en-US')}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default class History extends Component<{}> {
  state = {
    showDetails: false,
    detailsForTransaction: null,
    refreshing: false,
  }

  _setTransactionForDetails = (transaction) => {
    this.setState({
      detailsForTransaction: transaction,
      showDetails: true,
    });
  }

  _cancelDetails = () => {
    this.setState({
      showDetails: false,
    });
  }

  _onRefresh = () => {
    this.setState({refreshing: true});
    this.props.reloadHandler().then(() => {
      this.setState({refreshing: false});
    }, (e) => {
      console.log(e);
    });
  }

  render() {
    let showDetails = this.state.showDetails;
    let focusedTransaction = this.state.detailsForTransaction;

    return (
      <View style={styles.panelsWrap}>
        <View style={styles.panelsContainer}>
          <FlatList
            data={this.props.list}
            refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }
            renderItem={({item}) => 
              <Panel item={item} detailsHandler={this._setTransactionForDetails}/>
            }
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
        <TransactionDetails isVisible={showDetails}
          onBackdropPress={this._cancelDetails}
          handleCancel={this._cancelDetails}
          transaction={focusedTransaction} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  panelContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 15,
    borderBottomColor: '#fff',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  panelsContainer: {
    backgroundColor: 'rgba(255,255,255,0.8)',
    margin: 10,
  },
  panelsWrap: {
    backgroundColor: 'rgba(255,215,0, 0.5)',
    borderRadius: 10,
    margin: 15,
  },
  date: {
    fontSize: 10,
  },
  address: {
    fontFamily: 'monospace',
  },
  amount: {
    fontSize: 18,
    textAlign: 'right',
  },
  directionContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  amountContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  labelContainer: {
    margin: 5,
    flex: 1,
    justifyContent: 'center',
  },
  currencyLabel: {
    textAlign: 'left',
  },
  dateContainer: {
    flex: 1,
    flexDirection: 'column',
  }
});
