import React, { Component } from 'react';
import {
  View,
  TouchableHighlight,
  Text,
  StyleSheet,
} from 'react-native';

export default class GoldenButton extends Component<{}> {
  render() {
    let disabled = this.props.disabled;
    let look = styles.goldenButton;

    if (disabled) {
      look = styles.goldenButtonDisabled;
    }

    return (
      <TouchableHighlight style={look} {...this.props}>
        <View style={{transform: [{scaleX: 1/2}]}}>
          {this.props.children}
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  goldenButton: {
    justifyContent: 'center',
    backgroundColor: '#FFD700',
    width: 100,
    height: 50,
    borderRadius: 50,
    margin: 20,
    transform: [
      {scaleX: 2}
    ]
  },
  goldenButtonDisabled: {
    justifyContent: 'center',
    backgroundColor: '#DAA520',
    width: 100,
    height: 50,
    borderRadius: 50,
    margin: 20,
    transform: [
      {scaleX: 2}
    ]
  },
});
