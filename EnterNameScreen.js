import React, { Component } from 'react';
import {
  Text,
  View,
  TextInput,
  Platform,
  TouchableOpacity,
  StyleSheet
} from 'react-native';

import TreasuryModel from './Model/TreasuryModel';

import Spinner from 'react-native-loading-spinner-overlay';

const brandColor = '#FFDF00';

export default class EnterNameScreen extends Component<{}> {
  static navigationOptions = {
    header: null,
  }

  state = {
    name: '',
    number: null,
    spinner: false,
  };

  _onChangeText = (text) => {
    this.setState({
      name: text,
    });
  }

  _getSubmitAction = () => {
    this.setState({spinner: true});
    let name = this.state.name;
    let number = this.state.number;
    let model = new TreasuryModel();
    model.saveUser(number, name).then((userId) => {
      this.setState({spinner: false});
      const { navigate } = this.props.navigation;
      navigate('Loading', {
        firstTimeUser: true,
      });
    }, (e) => {
      console.log(e);
    });
  }

  componentDidMount() {
    const { params } = this.props.navigation.state;
    this.setState({
      number: params.number,
    });
  }

  render() {
    let headerText = "What's your full name?";
    let buttonText = "Submit user information";

    return (
      <View style={styles.container}>
        <Text style={styles.header}>{headerText}</Text>
        <View style={styles.nameContainer}>
          <TextInput
            name={'name' }
            type={'TextInput'}
            underlineColorAndroid={'transparent'}
            autoCapitalize={'words'}
            autoCorrect={false}
            onChangeText={this._onChangeText}
            placeholder={'Name'}
            keyboardAppearance={'dark'}
            style={styles.textInput}
            returnKeyType='go'
            autoFocus
            placeholderTextColor={brandColor}
            selectionColor={brandColor}
            maxLength={20}
            onSubmitEditing={this._getSubmitAction} />
          <TouchableOpacity style={styles.button} onPress={this._getSubmitAction}>
            <Text style={styles.buttonText}>{ buttonText }</Text>
          </TouchableOpacity>
          <Spinner
            visible={this.state.spinner}
            textContent={'One moment...'}
            textStyle={{ color: '#fff' }} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#040404',
  },
  nameContainer: {
    marginLeft: 20,
    marginRight: 20,
  },
  header: {
    textAlign: 'center',
    marginTop: 60,
    fontSize: 22,
    margin: 20,
    color: '#4A4A4A',
  },
  textInput: {
    padding: 0,
    margin: 0,
    fontSize: 20,
    color: brandColor
  },
  button: {
    marginTop: 20,
    height: 50,
    backgroundColor: brandColor,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  buttonText: {
    color: '#fff',
    fontFamily: 'Helvetica',
    fontSize: 16,
    fontWeight: 'bold'
  },
});
