import PhoneNumberScreen from './PhoneNumberScreen';
import SplashScreen from './SplashScreen';
import HomeScreen from './HomeScreen';
import MemberScreen from './MemberScreen';
import TreasurerScreen from './TreasurerScreen';
import LoadingScreen from './LoadingScreen';
import SpendScreen from './SpendScreen';
import EnterNameScreen from './EnterNameScreen';

import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';

import {
  StackNavigator,
} from 'react-navigation';
import { MessageBar, showMessage } from 'react-native-messages';

const StackNav = StackNavigator(
  {
    Splash: { screen: SplashScreen },
    Phone: { screen: PhoneNumberScreen },
    EnterName: { screen: EnterNameScreen },
    Home: { screen: HomeScreen },
    Loading: { screen: LoadingScreen },
    Member: { screen: MemberScreen },
    Treasurer: { screen: TreasurerScreen },
    Spend: { screen: SpendScreen },
  },
  {
    initialRouteName: 'Splash',
  }
);

export default class App extends Component<{}> {
  render() {
    return (
      <View style={styles.overall}>
        <StackNav/>
        <MessageBar/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  overall: {
    flex: 1,
  }
});
