import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
  Picker,
  Platform,
  TouchableOpacity,
  Clipboard,
  KeyboardAvoidingView,
  StyleSheet
} from 'react-native';

import Container from './Components/Container';
import GoldenButton from './Components/GoldenButton';
import Price from './Model/Price';
import {
  TreasuryCore
} from './NativeModules';
import { TEST, LOCAL } from './Constants';

import Icon from 'react-native-vector-icons/FontAwesome';
import { showMessage } from 'react-native-messages';

export default class SpendScreen extends Component<{}> {
  static navigationOptions = {
    header: null,
  }

  state = {
    address: null,
    amount: 0,
    usd: 0, // equivalent value in dollar
    value: 0, // satoshis
    multiplier: "100000", // mBTC
    tx_id: null,
    fee: null,
  }

  _onClickPaste = () => {
    Clipboard.getString().then((pastedItem) => {
      this.setState({
        address: pastedItem,
      });
    });
  }

  _onChangeAddress = (address) => {
    this.setState({
      address: address,
    });
  }

  _onChangeAmount = (amount) => {
    let newValue = amount * this.state.multiplier;
    let price = new Price();
    this.setState({
      amount: amount,
      value: newValue,
    });
    price.satoshiToUSD(newValue).then((usd) => {
      this.setState({
        usd: usd,
      });
    }, (e) => {
      console.log(e);
    });
  }

  _onChangeMultiplier = (multiplier) => {
    let newValue = this.state.amount * multiplier;
    this.setState({
      multiplier: multiplier,
      value: newValue,
    });
    let price = new Price();
    price.satoshiToUSD(newValue).then((usd) => {
      this.setState({
        usd: usd,
      });
    }, (e) => {
      console.log(e);
    });
  }

  _handleSend = () => {
    let amount = this.state.value.toString();
    let address = this.state.address;
    if (!address) {
      showMessage("Please specify address!");
    } else if (!amount) {
      showMessage("Please specify amount!");
    } else {
      TreasuryCore.createTransaction(amount, address).then((result) => {
        let tx_id = result.tx_id;
        let fee = result.fee;
        this.setState({
          tx_id: tx_id,
          fee: fee,
        });
        console.log(tx_id);
        console.log(fee);
      }, (e) => {
        console.log(e);
        showMessage(e.message);
      });
    }
  }

  render() {
    let dollarAmount = this.state.usd + " USD";
    let address = this.state.address;

    return (
      <Container>
        <KeyboardAvoidingView style={styles.pageContainer} enabled>
          <View style={styles.viewPort}>
            <View style={styles.addressPort}>
              <TextInput
                underlineColorAndroid={'transparent'}
                autoCapitalize={'none'}
                autoCorrect={false}
                onChangeText={this._onChangeAddress}
                placeholder={'Bitcoin Address'}
                placeholderTextColor={'#AAA'}
                keyboardAppearance={'dark'}
                style={styles.addressInput}
                value={address}
                returnKeyType='go'
                selectionColor={'#AAA'}
                maxLength={35} />
              <TouchableOpacity onPress={this._onClickPaste} style={styles.pasteButton}>
                <Icon color="black" name="paste" size={20} />
              </TouchableOpacity>
            </View>
            <View style={styles.amountPort}>
              <TextInput
                underlineColorAndroid={'transparent'}
                autoCapitalize={'none'}
                autoCorrect={false}
                onChangeText={this._onChangeAmount}
                keyboardType={Platform.OS === 'ios' ? 'number-pad' : 'numeric'}
                placeholder={'0.00'}
                placeholderTextColor={'#DDD'}
                keyboardAppearance={'dark'}
                style={styles.amountInput}
                returnKeyType='go'
                selectionColor={'#AAA'}
                maxLength={10} />
              <View style={styles.pickerPort}>
                <Picker
                  selectedValue={this.state.multiplier}
                  style={{ height: 50, width: 100 }}
                  onValueChange={this._onChangeMultiplier}>
                  <Picker.Item label="satoshi" value="1" />
                  <Picker.Item label="mBTC" value="100000" />
                  <Picker.Item label="BTC" value="100000000" />
                </Picker>
              </View>
            </View>
            <View style={styles.conversionPort}>
              <Text style={styles.conversionText}>{dollarAmount}</Text>
            </View>
          </View>
        </KeyboardAvoidingView>
        <View style={styles.buttonContainer}>
          <GoldenButton onPress={this._handleSend}>
            <Text style={styles.buttonText}>Send</Text>
          </GoldenButton>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  pageContainer: {
    flex: 1,
  },
  viewPort: {
  },
  addressPort: {
    alignItems: 'stretch',
    flexDirection: 'row',
    marginTop: 40,
    marginBottom: 40,
  },
  pasteButton: {
    backgroundColor: '#EEE',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 2,
    paddingLeft: 5,
    paddingRight: 5,
    borderRadius: 5,
  },
  amountPort: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignSelf: "stretch",
    backgroundColor: '#ffffe0',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    elevation: 10,
  },
  buttonContainer: {
    alignItems: 'center',
  },
  buttonText: {
    textAlign: 'center',
    color: 'white',
  },
  addressInput: {
    flex: 1,
    fontFamily: 'monospace',
    borderRadius: 5,
    backgroundColor: 'white',
  },
  amountInput: {
    flex: 1,
    marginLeft: 30,
    fontSize: 80,
    height: 300,
  },
  pickerPort: {
    marginLeft: 10,
    marginTop: 40,
    marginRight: 20,
  },
  conversionPort: {
    backgroundColor: '#add8e6',
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
  },
  conversionText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
  },
});
