import React, { Component } from 'react';
import { 
  View,
  Text,
  StyleSheet,
  AsyncStorage,
} from 'react-native';

import Container from './Components/Container';

import TreasuryModel from './Model/TreasuryModel';

export default class SplashScreen extends Component<{}> {
  static navigationOptions = {
    header: null,
  }

  constructor() {
    super();
    this.model = new TreasuryModel();
  }
  
  render() {
    return (
      <Container>
        <View style={styles.container}>
          <Text style={styles.txt}>Welcome!</Text>
        </View>
      </Container>
    );
  }

  componentDidMount() {
//    this._removeThis();
    this._fetchNumber();
  }

  _removeThis() {
    AsyncStorage.setItem('userId', '02d5b498-7c36-43f1-b2e1-b186cf19a08a', () => {
      this._fetchNumber();
    });
  }

  async _fetchNumber() {
    const { navigate } = this.props.navigation;
    this.model.userSaved().then((saved) => {
      if (saved) {
        navigate('Loading');
      } else {
        navigate('Phone');
      }
    }, (e) => {
      console.log(e);
    });
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txt: {
    color: 'white',
  }
});
