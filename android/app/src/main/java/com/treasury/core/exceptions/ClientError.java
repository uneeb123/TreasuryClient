package com.treasury.core.exceptions;

public class ClientError extends Exception {
    public ClientError(String msg) {
        super(msg);
    }
}
