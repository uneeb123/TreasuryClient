package com.treasury.core.model;

public class Treasury {
    public String _id;
    public String[] members;
    public String treasurer;
    public String created_by;
    public long spending_limit;
    public String[] shares;
    public TransactionHistory[] history;
    public long balance;
    public boolean ready;
    public String[] addresses;
    public String key_data;
    public long creation_time;

    Treasury(String _id,
             String[] members,
             String treasurer,
             String created_by,
             long spending_limit,
             String[] shares,
             TransactionHistory[] history,
             long balance,
             boolean ready,
             String[] addresses,
             String key_data,
             long creation_time) {
        this._id = _id;
        this.members = members;
        this.treasurer = treasurer;
        this.created_by = created_by;
        this.spending_limit = spending_limit;
        this.shares = shares;
        this.history = history;
        this.balance = balance;
        this.ready = ready;
        this.addresses = addresses;
        this.key_data = key_data;
        this.creation_time = creation_time;
    }
}