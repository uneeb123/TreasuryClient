package com.treasury.core.exceptions;

import java.util.Date;

public class AmountExceedsLimitException extends Exception {

    public long limit;
    public long nextWindow;

    public AmountExceedsLimitException(long satoshis, long nextWindow) {
        super("Amount exceeds the limit (" + satoshis + " satoshis) set by treasury. Try again at " +
                new Date(nextWindow).toString());
        this.limit = satoshis;
        this.nextWindow = nextWindow;
    }

    public AmountExceedsLimitException(long satoshis) {
        super("Amount exceeds the limit (" + satoshis + " satoshis) set by treasury.");
        this.limit = satoshis;
    }
}
