package com.treasury.core.model;

import java.util.Comparator;

public class TransactionHistoryByDate implements Comparator<TransactionHistory> {
    @Override
    public int compare(TransactionHistory t1, TransactionHistory t2) {
        long difference = t1.timestamp - t2.timestamp;
        if (difference < 0) {
            return -1;
        }
        else if (difference > 0) {
            return 1;
        }
        else {
            return 0;
        }
    }
}
