package com.treasury.core;

import android.util.Log;

import com.treasury.core.exceptions.AmountExceedsLimitException;
import com.treasury.core.exceptions.ClientError;
import com.treasury.core.exceptions.InitiationSequenceException;
import com.treasury.core.model.TransactionHistory;

import org.bitcoinj.core.*;
import org.bitcoinj.core.listeners.DownloadProgressTracker;
import org.bitcoinj.kits.WalletAppKit;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.params.TestNet3Params;
import org.bitcoinj.wallet.DeterministicSeed;
import org.bitcoinj.wallet.Wallet;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TreasuryCore {
    public static final long MINUTE = 60000;
    public static final long HOUR = MINUTE * 60;
    public static final long DAY = HOUR * 24;
    public static final long WEEK = DAY * 7;

    private static TreasuryCore instance;

    private static final String TAG = "TreasuryCore";

    private File directory;
    private String filePrefix;
    private WalletAppKit kit;
    private Controller controller;
    private NetworkParameters params;

    public static TreasuryCore getInstance(String filePrefix, File directory, boolean test) {
        if (instance == null) {
            instance = new TreasuryCore(filePrefix, directory, test);
        }
        return instance;
    }

    private TreasuryCore(String filePrefix, File directory, boolean test) {
        this.filePrefix = filePrefix;
        this.directory = directory;
        if (test) {
            params = TestNet3Params.get();
        } else {
            params = MainNetParams.get();
        }
        kit = new WalletAppKit(params, directory, filePrefix);
    }

    public void initiateKit(DownloadProgressTracker downloadListener) {
        if (kit.isRunning()) {
            Log.i(TAG, "Service still running");
            kit.stopAsync();
            kit.awaitTerminated();
            // reinitialize the service
            kit = new WalletAppKit(params, directory, filePrefix);
        }
        kit.setDownloadListener(downloadListener);
        kit.setBlockingStartup(false);
        kit.startAsync();
    }

    public void initiateTreasury(String treasuryId, boolean local) {
        if (kit == null) {
            throw new InitiationSequenceException();
        }
        controller = new Controller(treasuryId, kit.wallet(), local);
    }

    public void syncTreasury(boolean treasurer) throws IOException, ClientError {
        if (controller == null) {
            throw new InitiationSequenceException();
        }
        boolean treasuryUpdated = controller.syncTreasury();
        if (treasuryUpdated) {
            postFreshAddress();
        }
        postBalance();
        if (treasurer) {
            setupWatch(); // will post if not there
        }
    }

    public Transaction createTransaction(Coin value, String to)throws
            IOException, ClientError, InsufficientMoneyException, AmountExceedsLimitException {
        try {
            if (!controller.complyWithAccessControls(value)) {
                // should never get here
                return null;
            }
            Wallet wallet = kit.wallet();
            Log.i(TAG, "Balance: " + wallet.getBalance().toFriendlyString());
            Address toAddr = Address.fromBase58(wallet.getParams(), to);
            Wallet.SendResult result = wallet.sendCoins(kit.peerGroup(), toAddr, value);
            String tx_id = result.tx.getHashAsString();
            Date today = new Date();
            TransactionHistory newItem = new TransactionHistory(
                    to,-value.value, today.getTime(), tx_id, result.tx.getFee().value
            );
            controller.postTransaction(newItem);
            return result.tx;
        } catch (AmountExceedsLimitException e) {
            System.out.println(e.getMessage());
            throw e;
        }
    }

    public String postFreshAddress() throws IOException, ClientError {
        Address newAddress = kit.wallet().freshReceiveAddress();
        controller.postAddress(newAddress.toString());
        kit.wallet().addWatchedAddress(newAddress);
        return newAddress.toString();
    }

    public void postBalance() throws IOException, ClientError {
        Coin balance = kit.wallet().getBalance();
        controller.postBalance(balance.value);
    }

    public boolean setupWatch() throws IOException, ClientError {
        return controller.setupWatch();
    }

    /**
     * Not implemented at the moment, but high priority
     */
    public void getMnemonicCode() {
        DeterministicSeed seed = kit.wallet().getKeyChainSeed();
        seed.getCreationTimeSeconds();
        seed.getMnemonicCode();
    }

    public void restoreWallet(List<String> mnemonicCode, long creationTime) {
        String passphrase = "";
        DeterministicSeed seed = new DeterministicSeed(mnemonicCode, null, passphrase, creationTime);
        kit.restoreWalletFromSeed(seed);
    }
}
