package com.treasury;

import android.support.annotation.Nullable;
import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.treasury.core.TreasuryCore;
import com.treasury.core.exceptions.AmountExceedsLimitException;
import com.treasury.core.exceptions.ClientError;

import org.bitcoinj.core.Coin;
import org.bitcoinj.core.InsufficientMoneyException;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.core.listeners.DownloadProgressTracker;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class TreasuryWallet extends ReactContextBaseJavaModule {
    private TreasuryCore core;

    private static final String TAG = "TreasuryCore";
    private static String WALLETNAME;

    private static boolean serverRunningLocally = true;
    private static boolean usingTestNet = true;

    public TreasuryWallet(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "TreasuryCore";
    }

    @ReactMethod
    public void useTestNet(boolean testNet) {
        usingTestNet = testNet;
    }

    @ReactMethod
    public void useLocal(boolean local) {
        serverRunningLocally = local;
    }

    @ReactMethod
    public void initiateSync(final String treasuryId, final Promise promise) {
        WALLETNAME = treasuryId;
        core = TreasuryCore.getInstance(
                WALLETNAME, getReactApplicationContext().getFilesDir().getParentFile(), usingTestNet);
        // executes an asynchronous task
        Log.i(TAG, "Initiating blockchain sync");
        core.initiateKit(new DownloadProgressTracker() {
            @Override
            protected void startDownload(int blocks) {
                super.startDownload(blocks);
                Log.i(TAG, "Starting download");
                sendEvent("chainStart", null);
            }

            @Override
            protected void progress(double pct, int blocksSoFar, Date date) {
                super.progress(pct, blocksSoFar, date);
                Log.i(TAG, "Blockchain download completed " + pct + "%");
                WritableMap params = Arguments.createMap();
                params.putDouble("percent", pct);
                sendEvent("chainPercent", params);
            }

            @Override
            protected void doneDownload() {
                super.doneDownload();
                Log.i(TAG, "Download done");

                sendEvent("chainCompleted", null);

                Log.i(TAG, "Initiating Treasury");
                core.initiateTreasury(treasuryId, serverRunningLocally);
                try {
                    Log.i(TAG, "Syncing Treasury");
                    core.syncTreasury(true);
                    Log.i(TAG, "Done with syncing");
                    promise.resolve(null);
                } catch (IOException e) {
                    promise.reject(e);
                } catch (ClientError clientError) {
                    promise.reject(clientError);
                }
            }
        });
    }

    @ReactMethod
    public void firstTimeRun(Promise promise) {
        File directory = getReactApplicationContext().getFilesDir().getParentFile();
        File chainFile = new File(directory, WALLETNAME + ".spvchain");
        WritableMap params = Arguments.createMap();
        params.putBoolean("firstTimeRun", !chainFile.exists());
        promise.resolve(params);
    }

    @ReactMethod
    public void freshAddress(Promise promise) {
        try {
            String address = core.postFreshAddress();
            WritableMap params = Arguments.createMap();
            params.putString("address", address);
            promise.resolve(params);
        } catch (IOException e) {
            promise.reject(e);
        } catch (ClientError clientError) {
            promise.reject(clientError);
        }
    }

    @ReactMethod
    public void createTransaction(String valueStr, String address, Promise promise) {
        long value = Long.parseLong(valueStr);
        Log.i(TAG, "Creating transaction of " + value + " to " + address);
        Coin amount = Coin.valueOf(value);
        try {
            Transaction t = core.createTransaction(amount, address);
            WritableMap params = Arguments.createMap();
            String fee = String.valueOf(t.getFee().value);
            params.putString("fee", fee);
            params.putString("tx_id", t.getHashAsString());
            core.postFreshAddress();
            promise.resolve(params);
        } catch (IOException e) {
            promise.reject(e);
        } catch (ClientError clientError) {
            promise.reject(clientError);
        } catch (InsufficientMoneyException e) {
            promise.reject(e);
        } catch (AmountExceedsLimitException e) {
            promise.reject(e);
        } catch (Exception e) {
            promise.reject(e);
        }
    }

    private void sendEvent(String eventName,
                           @Nullable WritableMap params) {
        getReactApplicationContext().getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }
}