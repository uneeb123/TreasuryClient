package com.treasury;

import android.support.annotation.Nullable;
import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.treasury.core.TreasuryClient;
import com.treasury.core.TreasuryCore;
import com.treasury.core.exceptions.AmountExceedsLimitException;
import com.treasury.core.exceptions.ClientError;
import com.treasury.core.model.Treasury;

import org.bitcoinj.core.Coin;
import org.bitcoinj.core.InsufficientMoneyException;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.core.listeners.DownloadProgressTracker;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.params.TestNet3Params;
import org.bitcoinj.wallet.Wallet;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class MemberWallet extends ReactContextBaseJavaModule {
    private TreasuryCore core;

    private static final String TAG = "TreasuryCore";
    private static String WALLETNAME;
    private static File directory;
    private static File walletFile;

    private static boolean serverRunningLocally = true;
    private static boolean usingTestNet = true;

    public MemberWallet(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "MemberWallet";
    }

    @ReactMethod
    public void useTestNet(boolean testNet) {
        usingTestNet = testNet;
    }

    @ReactMethod
    public void useLocal(boolean local) {
        serverRunningLocally = local;
    }

    @ReactMethod
    public void initiateSync(final String treasuryId, final Promise promise) {
        WALLETNAME = treasuryId;
        directory = getReactApplicationContext().getFilesDir().getParentFile();
        core = TreasuryCore.getInstance(
                WALLETNAME, directory, usingTestNet);
        try {
            retrieveWatchingWallet();
        } catch (IOException e) {
            promise.reject(e);
        } catch (ClientError clientError) {
            promise.reject(clientError);
        }
        if (walletFile == null || !walletFile.exists()) {
            promise.reject(new Exception("Setup wallet before syncing"));
        }
        // executes an asynchronous task
        Log.i(TAG, "Initiating blockchain sync");
        core.initiateKit(new DownloadProgressTracker() {
            @Override
            protected void startDownload(int blocks) {
                super.startDownload(blocks);
                Log.i(TAG, "Starting download");
                sendEvent("chainStart", null);
            }

            @Override
            protected void progress(double pct, int blocksSoFar, Date date) {
                super.progress(pct, blocksSoFar, date);
                Log.i(TAG, "Blockchain download completed " + pct + "%");
                WritableMap params = Arguments.createMap();
                params.putDouble("percent", pct);
                sendEvent("chainPercent", params);
            }

            @Override
            protected void doneDownload() {
                super.doneDownload();
                Log.i(TAG, "Download done");

                sendEvent("chainCompleted", null);

                Log.i(TAG, "Initiating Treasury");
                core.initiateTreasury(treasuryId, serverRunningLocally);
                try {
                    Log.i(TAG, "Syncing Treasury");
                    core.syncTreasury(false);
                    Log.i(TAG, "Done with syncing");
                    promise.resolve(null);
                } catch (IOException e) {
                    promise.reject(e);
                } catch (ClientError clientError) {
                    promise.reject(clientError);
                }
            }
        });
    }

    @ReactMethod
    public void firstTimeRun(Promise promise) {
        File chainFile = new File(directory, WALLETNAME + ".spvchain");
        WritableMap params = Arguments.createMap();
        params.putBoolean("firstTimeRun", !chainFile.exists());
        promise.resolve(params);
    }

    @ReactMethod
    public void freshAddress(Promise promise) {
        try {
            String address = core.postFreshAddress();
            WritableMap params = Arguments.createMap();
            params.putString("address", address);
            promise.resolve(params);
        } catch (IOException e) {
            promise.reject(e);
        } catch (ClientError clientError) {
            promise.reject(clientError);
        }
    }

    /*
        TEMPORARY - should live inside other components
     */
    private static void retrieveWatchingWallet()
            throws IOException, ClientError {
        walletFile = new File(directory, WALLETNAME + ".wallet");
        if (!walletFile.exists()) {
            TreasuryClient client = new TreasuryClient(serverRunningLocally, WALLETNAME);
            Treasury treasury = client.getTreasury();
            String keyPubB58 = treasury.key_data;
            long creationTime = treasury.creation_time;
            NetworkParameters params;
            if (usingTestNet) {
                params = TestNet3Params.get();
            } else {
                params = MainNetParams.get();
            }
            Wallet watchingWallet = Wallet.fromWatchingKeyB58(params, keyPubB58, creationTime);
            watchingWallet.saveToFile(walletFile);
        } else {
            Log.i(TAG, "Wallet file found");
        }
    }

    private void sendEvent(String eventName,
                           @Nullable WritableMap params) {
        getReactApplicationContext().getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }
}